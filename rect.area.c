#include<stdio.h>
#include<math.h>
struct points
{
    float x,y;
};
struct rectangle
{
    struct points p1,p2,p3;
};
typedef struct rectangle rect;
typedef struct points pon;
int no_of_rect(int);
rect inputpoints(int,rect r[]);
float distance(pon,pon);
float area(float,float,float);
void output(rect,float);
int main()
{
    int n=0;
    float d1,d2,d3;
    n=no_of_rect(n);
    rect r[n];
    float a[n];
    r[n]=inputpoints(n,r);
    for(int i=0;i<n;i++)
    {
        d1=distance(r[i].p1,r[i].p2);
        d2=distance(r[i].p2,r[i].p3);
        d3=distance(r[i].p1,r[i].p3);
        a[i]=area(d1,d2,d3);
        output(r[i],a[i]);
    }
    return 0;
}
int no_of_rect(int n)
{
    scanf("%d",&n);
    return n;
}
rect inputpoints(int n,rect r[n])
{
    for(int i=0;i<n;i++)
    {
        //printf("Enter the point in x y format:\n");
        scanf("%f %f",&r[i].p1.x,&r[i].p1.y);
        scanf("%f %f",&r[i].p2.x,&r[i].p2.y);
        scanf("%f %f",&r[i].p3.x,&r[i].p3.y);
    }
}
float distance(pon p4,pon p5)
{
    float d;
    d=sqrt(pow(p5.x-p4.x,2)+pow(p5.y-p4.y,2));
    return d;
}
float area(float a,float b,float c)
{
    float r,ar;
    r=(a>b)?((a>c)?a:c):((b>c)?b:c);
    ar=(r==a)?(b*c):(r==b)?(a*c):(a*b);
    return ar;
}
void output(rect R,float a)
{
    printf("The area of the rectangle with vertices (%.f,%.f),(%.f,%.f)&(%.f,%.f) is %.f\n",R.p1.x,R.p1.y,R.p2.x,R.p2.y,R.p3.x,R.p3.y,a);
}
