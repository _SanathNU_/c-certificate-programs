#include<stdio.h>
#include<math.h>
void input(float *,float *);
float distance(float ,float ,float ,float );
void output(float,float,float,float,float);
int main()
{
float x1,x2,y1,y2,r;
input(&x1,&y1);
input(&x2,&y2);
r=distance(x1,y1,x2,y2);
output(x1,y1,x2,y2,r);
return 0;
}

void input(float *x,float *y)
{
printf("Enter the x and y coordiantes:\n");
scanf("%f %f",x,y);
}
float distance(float a,float b,float c,float d)
{
float s;
s=sqrt(pow(a-c,2)+pow(d-b,2));
return s;
}
void output(float x1,float y1,float x2,float y2,float r)
{
printf("The distance between the points (%.f,%.f) and (%.f,%.f) is %f",x1,y1,x2,y2,r);
}
