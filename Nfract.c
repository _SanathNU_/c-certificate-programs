#include<stdio.h>
struct fraction
{
    int n,d;
};
typedef struct fraction fract;
void input(int*);
fract inputfract(int,fract[]);
fract compute(int,fract[],fract);
fract simplify(fract);
void output(fract);
int main()
{
    int n;
    input(&n);
    fract f[n];
    f[n]=inputfract(n,f);
    fract fin;
    fin=compute(n,f,fin);
    fin=simplify(fin);
    output(fin);
    return 0;
}
void input(int *a)
{
    printf("Enter the number of fractions:\n");
    scanf("%d",a);
}
fract inputfract(int n,fract f[n])
{
    printf("Enter the fractions:\n");
    for(int i=0;i<n;i++)
    {
        printf("Enter the fraction in a/b form:\n");
        scanf("%d/%d",&f[i].n,&f[i].d);
    }
    return f[n];
}
fract compute(int n,fract f[n],fract fin)
{
    fin.n=f[0].n;
    fin.d=f[0].d;
    for(int i=1;i<n;i++)
    {
        fin.n=(fin.n*f[i].d)+(fin.d*f[i].n);
        fin.d=fin.d*f[i].d;
    }
    return fin;
}
fract simplify(fract fin)
{
    int g=1;
    for(int i=2;i<=fin.n && i<=fin.d;i++)
    {
        if(fin.n%i==0 && fin.d%i==0)
            g=i;
    }
    fin.n=fin.n/g;
    fin.d=fin.d/g;
    return fin;
}
void output(fract fin)
{
    printf("The sum of the fractions are %d/%d\n",fin.n,fin.d);
}
