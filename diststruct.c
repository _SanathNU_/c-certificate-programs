#include<stdio.h>
#include<math.h>
struct points
{
    float x;
    float y;
};
void output(struct points ,struct points ,float*);
void input(struct points *,struct points *);
float distance(struct points p1,struct points p2);
int main()
{
    float d;
    struct points p1,p2;
    input(&p1,&p2);
    d=distance(p1,p2);
    output(p1,p2,&d);
    return 0;
}
float distance(struct points p1,struct points p2)
{
    float r;
    r=sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));
    return r;
}
void input(struct points *p1,struct points *p2)
{
    printf("Enter the first points:\n");
    scanf("%f%f",&p1->x,&p1->y);
    printf("Enter the second points:\n");
    scanf("%f%f",&p2->x,&p2->y);
}
void output(struct points p1,struct points p2,float *a)
{
    printf("The distance between the points (%.f,%.f) and (%.f,%.f) is %f",p1.x,p1.y,p2.x,p2.y,*a);
}
