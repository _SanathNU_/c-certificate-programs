#include<stdio.h>
struct fraction
{
    int n;
    int d;
};
void input(int*,int*);
void fractsum(struct fraction f1,struct fraction f2,struct fraction *f3);
void simplyfy(int *,int*);
void output(struct fraction f1,struct fraction f2,struct fraction f3);
int main()
{
    printf("\nFunction to add two fractions\n");
    struct fraction f1,f2,f3;
    input(&f1.n,&f1.d);
    input(&f2.n,&f2.d);
    fractsum(f1,f2,&f3);
	simplyfy(&f3.n,&f3.d);
    output(f1,f2,f3);
    return 0;
}
void input(int *a,int *b)
{
    printf("Enter the numerator and denominator of the fraction:\n");
    scanf("%d%d",a,b);
}
void fractsum(struct fraction f1,struct fraction f2,struct fraction *f3)
{
    f3->d=(f1.d*f2.d);
    f3->n=(f1.n*f2.d)+(f2.n*f1.d);
}
void simplyfy(int*p,int*q)
{
	int g=0;
	for(int i=1;i<=*p && i<=*q;i++)
	{
		if(*p%i==0 && *q%i==0)
			g=i;
	}
	*p=*p/g;
	*q=*q/g;

}
void output(struct fraction f1,struct fraction f2,struct fraction f3)
{
    if(f3.n==1 && f3.d==1)
		printf("The sum of the fractions is 1");
		
	else
		printf("The sum of the fractions %d/%d and %d/%dis %d/%d\n",f1.n,f1.d,f2.n,f2.d,f3.n,f3.d);
}
