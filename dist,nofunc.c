#include<stdio.h>
#include<math.h>
int main()
{

    float d,x1,x2,y1,y2;
    printf("Enter the first coordinates:x1,y1:\n");
    scanf("%f%f",&x1,&x2);
    printf("Enter the second coordinates:x2,y2:\n");
    scanf("%f%f",&y1,&y2);
    d=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
    printf("The distance between the points (%.1f,%.1f) and (%.1f,%.1f) is %f\n",x1,y1,x2,y2,d);
    return 0;
}
