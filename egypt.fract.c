#include<stdio.h>
struct fraction
{
    int n;
    int d;
};
typedef struct fraction frac;
struct FractionS
{
    int a;
    int r[20];
    frac F;
};
typedef struct FractionS egyptian_fraction;
int no_of_it(int);
int gcd(int a,int b)
{
        while(b>0)
        {
            int temp;
            temp=b;
            b=a%b;
            a=temp;
        }
        return a;
}
int no_of_terms(int);
frac denominators(int,frac[]);
frac fraction_sum(int,frac[],frac);
void output(int,frac[],frac);
int main()
{
    int n;
    int i=0;
    n=no_of_it(n);
    egyptian_fraction s[n];
    //input loop
    while(i<n)
    {
        int t;
        frac fin;
        t=no_of_terms(t);
        frac f[t];
        f[t]=denominators(t,f);
        fin=fraction_sum(t,f,fin);
        s[i].a=t;
        s[i].F=fin;
        for(int a=0;a<t;a++)
        {
            s[i].r[a]=f[a].d ;
        }
        i++;
    }
    for(int i2=0;i2<n;i2++)
    {
        int t;
        t=s[i2].a;
        frac f[t];
        for(int j=0;j<t;j++)
        {
            f[j].d=s[i2].r[j];
            f[j].n=1;
        }
        output(t,f,s[i2].F);
    }

    return 0;
}
int no_of_it(int n)
{
    scanf("%d",&n);
    return n;
}
int no_of_terms(int t)
{
    scanf("%d",&t);
    return t;
}
frac denominators(int t,frac f[t])
{
    for(int i=0;i<t;i++)
    {
        scanf("%d",&f[i].d);
        f[i].n=1;
    }
    return f[t];
}
frac fraction_sum(int n,frac f[n],frac finale)
{
    finale.d=f[0].d;
    finale.n=1;
    for(int i=1;i<n;i++)
    {
        int g,lcm;
        g=gcd(finale.d,f[i].d);
        lcm=(finale.d*f[i].d)/g;
        finale.n=finale.n*(lcm/finale.d)+f[i].n*(lcm/f[i].d);
        finale.d=lcm;
    }
    int g=1;
    g=gcd(finale.n,finale.d);
    finale.n=finale.n/g;
    finale.d=finale.d/g;
    return finale;
}
void output(int t,frac f[t],frac fin)
{
    for(int i=0;i<t;i++)
    {
        if(i!=t-1)
            printf("%d/%d+",f[i].n,f[i].d);
        else
            printf("%d/%d=",f[i].n,f[i].d);
    }
    printf("%d/%d\n",fin.n,fin.d);
}
